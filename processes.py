import config

def ampli(sound, factor: float):
    """Función que amplifica el sonido"""
    # Multiplica el valor de cada muestra por un número dado. Si el resultado de la multiplicación es mayor que la
    # amplitud máxima o menor que la amplitud mínima, el valor de la muestra amplificada es ese máximo o ese mínimo.
    for n in range(len(sound)): # Bucle que itera cada elemento (muestra) hasta el final del sonido proporcionado
        if int(sound[n] * factor) > config.max_amp: # Condición multiplicación mayor que la amplitud máxima
            sound[n] = config.max_amp
        elif int(sound[n] * factor) < -config.max_amp: # Condición multiplicación menor que la amplitud mínima
            sound[n] = -config.max_amp
        else:
            sound[n] = int(sound[n] * factor)  # Si no se cumple lo anterior, la amplitud será el resultado de
                                               # la multiplicación
    return sound



def reduce(sound, factor: int):
    """Función que reduce el número de muestras de sonido"""
    for n in range(0, len(sound), factor): # Bucle que itera cada elemento (muestra) según el factor proporcionado
                                           # Ejemplo: factor = 2, n = 0,2,4,6...
        if factor > 0: # Condición factor positivo
            sound[n] = False # Reduce cada muestra (n) del sonido proporcionado
    return sound



def extend(sound, factor: int):
    """Función que extiende el número de muestras de sonido"""
    r = 0 # Constante para indicar posición
    for n in range(len(sound)): # Bucle que itera cada elemento (muestra) hasta el final del sonido proporcionado
        if factor > 0: # Condición factor positivo
            if n % factor == 0: # Condición de que el resto de la división entre el número de muestra y el factor sea 0
                                # Ejemplo: factor = 3, 3 % 3 = 0, 6 % 3 = 0, 9 % 3 = 0...
                media = int((sound[n] + sound[n+1])/2) # Valor de la nueva muestra a insertar (media)
                sound.insert(n+r, media) # Se inserta en la siguiente posición a n el valor de la nueva muestra
                r += 1 # Incremento posición
    return sound





    
