"""Methods for storing sound, or playing it"""
import soundfile

import config

import sounddevice


def play(sound):
    sounddevice.play(sound, config.samples_second)
    sounddevice.wait()


def _mean(sound):
    total = 0
    for sample in sound:
        total += abs(sample)
    return int(total / len(sound))


def draw(sound, period: float):
    samples_period = int(period * config.samples_second)
    for nsample in range(0, len(sound), samples_period):
        chunk = sound[nsample: nsample + samples_period]
        mean_sample = _mean(chunk)
        stars = mean_sample // 1000
        print('*' * stars)

def store(sound, path: str):
    """"Función que almacena el sonido"""
    # Almacena un array de muestras de sonido (sound) en un fichero (nombre completo: path)
    soundfile.write(path, sound, config.samples_second)
    print(f"Archivo guardado en {path}") # Muestra por pantalla que el archivo se ha guardado correctamente en un
                                         # determinado fichero


