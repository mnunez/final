"""Methods for producing sound (sound sources)"""

import array
import math
import config
import soundfile
import sounddevice


def _samples(duration):
    """Compute the number of samples for a certain duration (sec)"""

    # Total number of samples (samples per second by number of seconds)
    return int(config.samples_second * duration)


def sin(duration: float, freq: float):
    """Produce a list of samples of a sinusoidal signal, of duratioon (sec)"""

    nsamples = _samples(duration)

    # Create an array of integers (signed shorts: 16 bit signed integers)
    sound = array.array('h', [0] * nsamples)

    # Compute sin samples (maximum amplitude, freq frequency)
    for nsample in range(nsamples):
        t = nsample / config.samples_second
        sound[nsample] = int(config.max_amp *
                             math.sin(2 * config.pi * freq * t))

    return sound


def constant(duration: float, positive: bool):
    """Produce a list of samples of a constant signal, of duration (sec)"""

    nsamples = _samples(duration)

    # Create an array of integers with either max or min amplitude
    if positive:
        sample = config.max_amp
    else:
        sample = - config.max_amp

    sound = array.array('h', [sample] * nsamples)

    return sound


def square(duration: float, freq: float):
    """Produce a list of samples of a square signal, of duration (sec)"""

    nsamples = _samples(duration) # Número de muestras de la señal cuadrada
    sound = array.array('h', [0] * nsamples) # Sonido ---> vector de muestras
    for n in range(nsamples): # Bucle que recorre cada elemento (muestra) de uno en uno del conjunto de muestras
        # Para indicar el tiempo que está la señal a máxima amplitud y a mínima amplitud, se calcula el número
        # de semiciclos
        t_2 = 1 / (freq * 2)  # Duración de la mitad de cada período de la señal
        t = n / config.samples_second # Duración de un período de la señal (número de muestras / muestras por segundo)
        nmitad = int(t / t_2) # Número de mitades de período de la señal (semiciclos)
        # Se considera que siempre que el número de semiciclos de la señal sea impar la amplitud es mínima
        # y viceversa; de esta forma, la señal cuadrada estará durante el mismo tiempo a la máxima amplitud y durante
        # el mismo tiempo a la mínima amplitud.
        if (nmitad % 2) != 0:
            sound[n] = -config.max_amp
        else:
            sound[n] = config.max_amp
    return sound



# FUNCIONALIDAD OPCIONAL

def readfile(duration: float, path: str):
    """Lee el sonido de un fichero en formato WAV"""
    data, fs = soundfile.read(path, dtype='int16') # Lectura y frecuencia de muestreo
    period = int(duration * fs) # Período de muestras
    for n in range(0, len(data), period): # Bucle que itera cada muestra hasta el final según el período proporcionado
        sound = data[n: n + period] # Lista de muestras (final)
        sounddevice.play(sound, fs) # Play sonido
        sounddevice.wait()



# FUNCIONALIDAD OPCIONAL PROPIA

def cos(duration: float, freq: float):
    """Produce a list of samples of a sinusoidal signal (cos), of duration (sec)"""

    nsamples = _samples(duration)

    # Create an array of integers (signed shorts: 16 bit signed integers)
    sound = array.array('h', [0] * nsamples)

    # Compute sin samples (maximum amplitude, freq frequency)
    for nsample in range(nsamples):
        t = nsample / config.samples_second
        sound[nsample] = int(config.max_amp *
                             math.cos(2 * config.pi * freq * t))
    return sound


